package com.itheima.reggie.service;

import com.baomidou.mybatisplus.extension.service.IService;
import com.itheima.reggie.entity.Employee;
import org.springframework.stereotype.Service;

/**
 * @author yu yang
 * @date 2022/9/26 -20:29
 */

public interface EmployeeService extends IService<Employee> {
}
