package com.itheima.reggie.service;

import com.baomidou.mybatisplus.extension.service.IService;
import com.itheima.reggie.entity.User;

/**
 * @author yu yang
 * @date 2022/9/29 -22:18
 */

public interface UserSerivce extends IService<User> {
}
