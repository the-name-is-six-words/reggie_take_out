package com.itheima.reggie.service;

import com.baomidou.mybatisplus.extension.service.IService;
import com.itheima.reggie.entity.AddressBook;

/**
 * @author yu yang
 * @date 2022/9/30 -14:05
 */
public interface AddressBookService extends IService<AddressBook> {
}
