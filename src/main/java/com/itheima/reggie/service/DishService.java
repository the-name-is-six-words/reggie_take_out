package com.itheima.reggie.service;

import com.baomidou.mybatisplus.extension.service.IService;
import com.itheima.reggie.dto.DishDto;
import com.itheima.reggie.entity.Dish;

import java.util.List;

/**
 * @author yu yang
 * @date 2022/9/27 -20:25
 */
public interface DishService extends IService<Dish> {
//    保存菜品信息和口味信息
    void saveWithFlavor(DishDto dishDto);
//    获取菜品和口味信息
    DishDto getByIdWithFlavor(Long id);

    void updateDishAndDishFlavor(DishDto dishDto);

    void deleteWithDishFlavor(List<Long> ids);
}
