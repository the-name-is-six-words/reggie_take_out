package com.itheima.reggie.service.impl;

import com.baomidou.mybatisplus.extension.service.impl.ServiceImpl;
import com.itheima.reggie.entity.User;
import com.itheima.reggie.mapper.UserMapper;
import com.itheima.reggie.service.UserSerivce;
import org.springframework.stereotype.Service;

/**
 * @author yu yang
 * @date 2022/9/29 -22:18
 */
@Service
public class UserSerivceImpl extends ServiceImpl<UserMapper, User> implements UserSerivce {

}
