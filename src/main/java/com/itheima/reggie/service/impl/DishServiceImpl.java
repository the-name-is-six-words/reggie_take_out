package com.itheima.reggie.service.impl;

import com.baomidou.mybatisplus.core.conditions.query.LambdaQueryWrapper;
import com.baomidou.mybatisplus.extension.service.impl.ServiceImpl;
import com.itheima.reggie.common.CustomException;
import com.itheima.reggie.dto.DishDto;
import com.itheima.reggie.entity.Dish;
import com.itheima.reggie.entity.DishFlavor;
import com.itheima.reggie.mapper.DishMapper;
import com.itheima.reggie.service.CategoryService;
import com.itheima.reggie.service.DishFlavorService;
import com.itheima.reggie.service.DishService;
import org.springframework.beans.BeanUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import java.util.ArrayList;
import java.util.List;
import java.util.stream.Collectors;

/**
 * @author yu yang
 * @date 2022/9/27 -20:28
 */
@Service
public class DishServiceImpl extends ServiceImpl<DishMapper, Dish> implements DishService {
    @Autowired
    private DishFlavorService dishFlavorService;
    @Autowired
    private CategoryService categoryService;

    @Transactional
    @Override
    public void saveWithFlavor(DishDto dishDto) {
        //保存菜品信息
        this.save(dishDto);
        Long dishId = dishDto.getId();
        //为口味的dishId赋值
        List<DishFlavor> dishFlavors = dishDto.getFlavors();
        if (dishFlavors != null && !dishFlavors.isEmpty()) {
            dishFlavors = dishFlavors.stream().map(dishFlavor -> {
                dishFlavor.setDishId(dishId);
                return dishFlavor;
            }).collect(Collectors.toList());
            //保存口味信息
            dishFlavorService.saveBatch(dishFlavors);
        }
    }

    @Override
    public DishDto getByIdWithFlavor(Long id) {
        //根据id查询菜品信息并拷贝到dishDto中
        DishDto dishDto = new DishDto();
        Dish dish = this.getById(id);
        BeanUtils.copyProperties(dish, dishDto);
        //根据id查询口味信息
        LambdaQueryWrapper<DishFlavor> queryWrapper = new LambdaQueryWrapper<>();
        queryWrapper.eq(DishFlavor::getDishId, id);
        List<DishFlavor> dishFlavors = dishFlavorService.list(queryWrapper);

        dishDto.setFlavors(dishFlavors);
        return dishDto;
    }

    @Override
    public void updateDishAndDishFlavor(DishDto dishDto) {
        //修改菜品信息

        this.updateById(dishDto);
        //修改口味信息

        LambdaQueryWrapper<DishFlavor> queryWrapper = new LambdaQueryWrapper<>();
        queryWrapper.eq(DishFlavor::getDishId, dishDto.getId());
        dishFlavorService.remove(queryWrapper);

        List<DishFlavor> dishFlavors = dishDto.getFlavors();
        if (dishFlavors != null && !dishFlavors.isEmpty()) {
            dishFlavors = dishFlavors.stream().map(dishFlavor -> {
                dishFlavor.setDishId(dishDto.getId());
                return dishFlavor;
            }).collect(Collectors.toList());
            //保存口味信息
            dishFlavorService.saveBatch(dishFlavors);
        }

    }

    @Transactional
    @Override
    public void deleteWithDishFlavor(List<Long> ids) {
        LambdaQueryWrapper<Dish> queryWrapper = new LambdaQueryWrapper<>();
        queryWrapper.eq(Dish::getStatus,1).in(Dish::getId,ids);
        int count = this.count(queryWrapper);
        if (count > 0){
            throw new CustomException("该菜品在售卖中，请先停止售卖");
        }
        LambdaQueryWrapper<DishFlavor> queryWrapper1 = new LambdaQueryWrapper<>();
        queryWrapper1.in(DishFlavor::getDishId,ids);
        dishFlavorService.remove(queryWrapper1);

        this.removeByIds(ids);

    }
}
