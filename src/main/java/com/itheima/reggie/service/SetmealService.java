package com.itheima.reggie.service;

import com.baomidou.mybatisplus.extension.service.IService;
import com.itheima.reggie.dto.SetmealDto;
import com.itheima.reggie.entity.Setmeal;

import java.util.List;

/**
 * @author yu yang
 * @date 2022/9/27 -20:27
 */
public interface SetmealService extends IService<Setmeal> {
    //新增套餐以及菜品信息
    void saveWithDish(SetmealDto setmealDto);

    void deleteWithDish(List<Long> ids);

    SetmealDto getWithDish(Long id);

    void updateWithDish(SetmealDto setmealDto);
}
