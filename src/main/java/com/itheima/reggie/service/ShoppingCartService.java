package com.itheima.reggie.service;

import com.baomidou.mybatisplus.extension.service.IService;
import com.itheima.reggie.entity.ShoppingCart;

/**
 * @author yu yang
 * @date 2022/9/30 -14:43
 */
public interface ShoppingCartService extends IService<ShoppingCart> {
}
