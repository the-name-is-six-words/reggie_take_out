package com.itheima.reggie.service;

import com.baomidou.mybatisplus.extension.service.IService;
import com.itheima.reggie.entity.DishFlavor;

/**
 * @author yu yang
 * @date 2022/9/28 -15:50
 */
public interface DishFlavorService extends IService<DishFlavor> {
}
