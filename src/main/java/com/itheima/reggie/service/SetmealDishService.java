package com.itheima.reggie.service;

import com.baomidou.mybatisplus.extension.service.IService;
import com.itheima.reggie.entity.SetmealDish;

/**
 * @author yu yang
 * @date 2022/9/28 -21:10
 */
public interface SetmealDishService extends IService<SetmealDish> {
}
