package com.itheima.reggie.service;

import com.baomidou.mybatisplus.extension.service.IService;
import com.itheima.reggie.entity.Category;

/**
 * @author yu yang
 * @date 2022/9/27 -20:26
 */
public interface CategoryService extends IService<Category> {
    //删除分类信息并检查该分类下是否有相关菜品和套餐
    void deleteById(Long id);
}
