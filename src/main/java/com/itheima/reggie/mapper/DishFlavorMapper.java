package com.itheima.reggie.mapper;

import com.baomidou.mybatisplus.core.mapper.BaseMapper;
import com.itheima.reggie.entity.DishFlavor;
import org.apache.ibatis.annotations.Mapper;

/**
 * @author yu yang
 * @date 2022/9/28 -15:49
 */
@Mapper
public interface DishFlavorMapper extends BaseMapper<DishFlavor> {
}
