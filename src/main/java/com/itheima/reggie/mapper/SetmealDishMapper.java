package com.itheima.reggie.mapper;

import com.baomidou.mybatisplus.core.mapper.BaseMapper;
import com.itheima.reggie.entity.SetmealDish;
import org.apache.ibatis.annotations.Mapper;

/**
 * @author yu yang
 * @date 2022/9/28 -21:09
 */
@Mapper
public interface SetmealDishMapper extends BaseMapper<SetmealDish> {
}
