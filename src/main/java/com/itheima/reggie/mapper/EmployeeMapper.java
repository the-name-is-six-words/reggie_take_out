package com.itheima.reggie.mapper;

import com.baomidou.mybatisplus.core.mapper.BaseMapper;
import com.itheima.reggie.entity.Employee;
import org.apache.ibatis.annotations.Mapper;

/**
 * @author yu yang
 * @date 2022/9/26 -20:32
 */
@Mapper
public interface EmployeeMapper extends BaseMapper<Employee> {

}
