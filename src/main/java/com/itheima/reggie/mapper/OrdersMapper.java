package com.itheima.reggie.mapper;

import com.baomidou.mybatisplus.core.mapper.BaseMapper;
import com.itheima.reggie.entity.Orders;
import com.itheima.reggie.util.BaseContext;
import org.apache.ibatis.annotations.Mapper;

/**
 * @author yu yang
 * @date 2022/9/30 -16:51
 */
@Mapper
public interface OrdersMapper extends BaseMapper<Orders> {
}
