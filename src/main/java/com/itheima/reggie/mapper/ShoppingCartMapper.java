package com.itheima.reggie.mapper;

import com.baomidou.mybatisplus.core.mapper.BaseMapper;
import com.itheima.reggie.entity.ShoppingCart;
import org.apache.ibatis.annotations.Mapper;

/**
 * @author yu yang
 * @date 2022/9/30 -14:43
 */
@Mapper
public interface ShoppingCartMapper extends BaseMapper<ShoppingCart> {
}
