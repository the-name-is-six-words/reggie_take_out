package com.itheima.reggie.controller;

import com.baomidou.mybatisplus.core.conditions.query.LambdaQueryWrapper;
import com.baomidou.mybatisplus.core.conditions.update.LambdaUpdateWrapper;
import com.baomidou.mybatisplus.extension.plugins.pagination.Page;
import com.itheima.reggie.common.R;
import com.itheima.reggie.dto.DishDto;
import com.itheima.reggie.dto.SetmealDto;
import com.itheima.reggie.entity.Category;

import com.itheima.reggie.entity.Dish;
import com.itheima.reggie.entity.Setmeal;
import com.itheima.reggie.entity.SetmealDish;
import com.itheima.reggie.service.CategoryService;
import com.itheima.reggie.service.DishService;
import com.itheima.reggie.service.SetmealDishService;
import com.itheima.reggie.service.SetmealService;
import org.apache.commons.lang.StringUtils;
import org.springframework.beans.BeanUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.cache.annotation.CacheEvict;
import org.springframework.cache.annotation.Cacheable;
import org.springframework.web.bind.annotation.*;

import java.util.List;
import java.util.stream.Collectors;

/**
 * @author yu yang
 * @date 2022/9/28 -21:12
 */
@RestController
@RequestMapping("/setmeal")
public class SetmealController {
    @Autowired
    private SetmealService setmealService;
    @Autowired
    private CategoryService categoryService;
    @Autowired
    private SetmealDishService setmealDishService;
    @CacheEvict(value = "setmeal",allEntries = true)
    @PostMapping
    public R<String> save(@RequestBody SetmealDto setmealDto){
        setmealService.saveWithDish(setmealDto);
        return R.success("成功新增套餐");
    }
    @GetMapping("/page")
    public R<Page<SetmealDto>> getPage(Integer page,Integer pageSiez,String name){
        Page<Setmeal> setmealPage = new Page<>();
        Page<SetmealDto> setmealDtoPage = new Page<>();

        LambdaQueryWrapper<Setmeal> queryWrapper = new LambdaQueryWrapper<>();
        queryWrapper.like(StringUtils.isNotBlank(name),Setmeal::getName,name)
                        .orderByDesc(Setmeal::getUpdateTime);
        setmealService.page(setmealPage,queryWrapper);
        BeanUtils.copyProperties(setmealPage,setmealDtoPage,"records");

        List<Setmeal> setmealList = setmealPage.getRecords();
        List<SetmealDto> setmealDtoList = setmealList.stream().map(setmeal -> {
            SetmealDto setmealDto = new SetmealDto();
            BeanUtils.copyProperties(setmeal, setmealDto);
            Long categoryId = setmeal.getCategoryId();
            Category category = categoryService.getById(categoryId);
            if (category != null) {
                setmealDto.setCategoryName(category.getName());
            }
            return setmealDto;
        }).collect(Collectors.toList());
        setmealDtoPage.setRecords(setmealDtoList);
        return R.success(setmealDtoPage);
    }

    @CacheEvict(value = "setmeal",allEntries = true)
    @DeleteMapping
    public R<String> delete(@RequestParam List<Long> ids){
        setmealService.deleteWithDish(ids);
        return R.success("删除套餐成功");
    }

    @PostMapping("/status/{stat}")
    public R<String> updateStatus(@PathVariable("stat") Integer status, @RequestParam List<Long> ids) {
        LambdaUpdateWrapper<Setmeal> queryWrapper = new LambdaUpdateWrapper<>();
        queryWrapper.in(Setmeal::getId,ids)
                .set(Setmeal::getStatus,status);
        setmealService.update(queryWrapper);
        return R.success("成功修改状态");
    }
    @GetMapping("/{id}")
    public R<SetmealDto> getById(@PathVariable("id")Long id){
        SetmealDto setmealDto = setmealService.getWithDish(id);
        if (setmealDto !=null){
            return R.success(setmealDto);
        }
        return R.error("没有该对象");
    }
    @CacheEvict(value = "setmeal",allEntries = true)
    @PutMapping
    public R<String> update(@RequestBody  SetmealDto setmealDto){
        setmealService.updateWithDish(setmealDto);
        return R.success("修改套餐成功");
    }

    @Cacheable(value = {"setmeal"},key = "#setmealDto.categoryId+'_'+#setmealDto.status" ,unless = "#result.data.size() == 0")
    @GetMapping("/list")
    public R<List<Setmeal>> getSetmeal(SetmealDto setmealDto){
        LambdaQueryWrapper<Setmeal> queryWrapper = new LambdaQueryWrapper<>();
        queryWrapper.eq(setmealDto.getStatus() !=null, Setmeal::getStatus,setmealDto.getStatus())
                .eq(setmealDto.getCategoryId() != null,Setmeal::getCategoryId,setmealDto.getCategoryId());
        List<Setmeal> setmeals = setmealService.list(queryWrapper);
        return R.success(setmeals);
    }
    @CacheEvict(value = "setmeal",allEntries = true)
    @GetMapping("/dish/{id}")
    public R<SetmealDto> getDish(@PathVariable Long id){
        SetmealDto setmealDto = setmealService.getWithDish(id);
        if (setmealDto !=null){
            return R.success(setmealDto);
        }
        return R.error("没有该对象");
    }

}
