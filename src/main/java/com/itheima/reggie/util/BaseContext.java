package com.itheima.reggie.util;

/**
 * 使用ThreadLocal设置当前登录用户的id
 * @author yu yang
 * @date 2022/9/27 -17:05
 */
public class BaseContext {
    private static ThreadLocal<Long> threadLocal = new ThreadLocal<>();

    public static void setCurrentId(Long id){
        threadLocal.set(id);
    }
    public static Long getCurrentId(){
        return threadLocal.get();
    }
}
