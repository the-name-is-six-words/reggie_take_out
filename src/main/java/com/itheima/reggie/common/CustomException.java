package com.itheima.reggie.common;

/**
 * 自定义的异常
 * @author yu yang
 * @date 2022/9/27 -21:09
 */
public class CustomException extends RuntimeException{
    public CustomException(String msg){
        super(msg);
    }
}
